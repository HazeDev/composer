<?php

class ClassLoader
{
    public $namespace;

    public function __construct($namespace)
    {
        $this->namespace = $namespace;

        $this->register();
    }
    private function load($name,$namespace,$path)
    {
        $classPath = str_replace([$namespace,'\\'],[$path,'/'],$name) . ".php";

        if (file_exists($classPath)) {
            require $classPath;
        }
    }

    private function register()
    {
        spl_autoload_register(function($name) {
            foreach ($this->namespace as $namespace => $path) {
                if (strpos($name,$namespace) !== false) {
                    $this->load($name,$namespace,$path);
                }   
            }
        });
    }
    
}